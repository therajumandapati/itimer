angular.module('itimer.controllers', [])

.controller('DashboardCtrl', function($scope, $location, Projects) {
    $scope.projects = Projects.all();
    $scope.openNewTask = function() {
        $location.path('/home/new');
    };
})

.controller('ProjectCtrl', function($scope, $stateParams, Projects) {
    $scope.project = Projects.get($stateParams.projectId);
})

.controller('NewTimerCtrl', function($scope, $ionicPopup) {

    $scope.openModal = function() {
        var myPopup = $ionicPopup.show({
            templateUrl: 'templates/project-modal.html',
            title: 'Select project',
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }]
        });
        myPopup.then(function(res) {
            console.log('Tapped!', res);
        });
    };

})

.controller('AccountCtrl', function($scope) {});
angular.module('itimer.controllers', [])

.controller('DashboardCtrl', function($scope, $location, Projects) {
    $scope.projects = Projects.all();
    $scope.openNewTask = function() {
        $location.path('/home/new');
    };
})

.controller('ProjectCtrl', function($scope, $stateParams, Projects) {
    $scope.project = Projects.get($stateParams.projectId);
})

.controller('NewTimerCtrl', function($scope, ParseService, $ionicPopup) {

    var projectPopup;
    $scope.showPopup = function() {
        $scope.data = {}

        ParseService.getProjects().then(function(results) {
            console.log(results);
            $scope.projects = results;

        })

        projectPopup = $ionicPopup.show({
            templateUrl: 'templates/project-modal.html',
            title: 'Select project',
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }]
        });
        projectPopup.then(function(res) {
            console.log('Tapped!', res);
        });
    }

    $scope.closePopup = function(){
        projectPopup.close();
    }

    // $scope.openModal = function() {
    //     var myPopup = $ionicPopup.show({
    //         templateUrl: 'templates/project-modal.html',
    //         title: 'Select project',
    //         scope: $scope,
    //         buttons: [{
    //             text: 'Cancel'
    //         }]
    //     });
    //     myPopup.then(function(res) {
    //         console.log('Tapped!', res);
    //     });
    // };

})

.controller('AccountCtrl', function($scope) {});
