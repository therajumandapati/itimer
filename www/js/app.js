// Ionic itimer App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'itimer' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'itimer.services' is found in services.js
// 'itimer.controllers' is found in controllers.js
angular.module('itimer', ['ionic','angularParse', 'itimer.controllers', 'itimer.services'])

.run(function($ionicPlatform) {
    Parse.initialize("oUv1ELgoq3oF40vM32SXEb6GVz9y0uBHL2QcUtqu", "pKNZGh83HlUsfqY5rUoTYRNhPHJzVqibu6I3jM4C");

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
    .state('home', {
        url: "/home",
        abstract: true,
        templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('home.dashboard', {
        url: '/dashboard',
        views: {
            'home-dashboard': {
                templateUrl: 'templates/tab-dashboard.html',
                controller: 'DashboardCtrl'
            }
        }
    })

    .state('home.project-detail', {
        url: '/project/:projectId',
        views: {
            'home-dashboard': {
                templateUrl: 'templates/project-detail.html',
                controller: 'ProjectCtrl'
            }
        }
    })

    .state('home.new', {
        url: '/new',
        views: {
            'home-new': {
                templateUrl: 'templates/tab-new.html',
                controller: 'NewTimerCtrl'
            }
        }
    })

    .state('home.account', {
        url: '/account',
        views: {
            'home-account': {
                templateUrl: 'templates/tab-account.html',
                controller: 'AccountCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home/dashboard');

});
