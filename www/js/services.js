angular.module('itimer.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Projects', function() {
    // Might use a resource here that returns a JSON array

    // Some fake testing data
    var projects = [{
        id: 0,
        name: '4leafcafe',
        hours: 3,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }, {
        id: 1,
        name: 'backand',
        hours: 2,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }, {
        id: 2,
        name: 'naw',
        hours: 6,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }, {
        id: 3,
        name: 'libertyleap',
        hours: 6,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }, {
        id: 4,
        name: 'venkatapathirajum.com',
        hours: 6,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }, {
        id: 5,
        name: 'opensource',
        hours: 6,
        createdDate: '11/11/1992',
        tasks: [{
            id: 0,
            name: "seed project",
            hours: 1
        }, {
            id: 1,
            name: "second project",
            hours: 0.5
        }]
    }];

    return {
        all: function() {
            return projects;
        },
        get: function(projectId) {
            // Simple index lookup
            return projects[projectId];
        }
    }
})

.factory('ParseService', function(parseQuery) {
    var getProjects = function() {
        var query = parseQuery.new('Project');

        return parseQuery.find(query);

    }

    return{
        getProjects: getProjects
    }
});
